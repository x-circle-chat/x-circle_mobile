import { Text, View, StyleSheet } from "react-native";
import AppTheme from "../../../../constants/AppTheme";
import CustomFont from "../../../../constants/CustomFont";

const ChatHeader = ({ title }) => {
  return (
    <View style={styles.header}>
      <Text style={styles.text}>{title}</Text>
    </View>
  );
};

export default ChatHeader;

const styles = StyleSheet.create({
  header: {
    paddingTop: 16,
    alignItems: "center",
    backgroundColor: AppTheme.backgroundColor,
  },
  text: {
    color: AppTheme.subtitleTextColor,
    fontFamily: CustomFont.PoppinsRegular,
    fontSize: 12,
  },
});
