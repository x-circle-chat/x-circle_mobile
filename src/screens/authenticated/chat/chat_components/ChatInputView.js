import {
  StyleSheet,
  View,
  Pressable,
  Image,
  Text,
  Keyboard,
} from "react-native";
import AppTheme from "../../../../constants/AppTheme";
import CustomFont from "../../../../constants/CustomFont";
import AssetPath from "../../../../constants/AssetPath";
import AutoGrowTextInput from "../../../../components/AutoGrowTextInput";
import Strings from "../../../../constants/Strings";

const ChatInputView = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Image
        style={{
          width: 40,
          height: 40,
        }}
        source={AssetPath.attach}
      />
      <AutoGrowTextInput
        style={styles.input}
        //onChangeText={onChangeNumber}
        //value={number}
        placeholder={Strings.searchHere}
        selectionColor={AppTheme.primaryTextColor}
        placeholderTextColor={AppTheme.lightTextColor}
        returnKeyType="done"
        blurOnSubmit={true}
        onSubmitEditing={() => {
          Keyboard.dismiss();
        }}
      />
      <Image
        style={{
          width: 18,
          height: 18,
        }}
        resizeMode="contain"
        source={AssetPath.send_message}
      />
      <Image
        style={{
          width: 50,
          height: 50,
          marginLeft: 12,
        }}
        resizeMode="contain"
        source={AssetPath.record_audio}
      />
    </View>
  );
};
export default ChatInputView;

const styles = StyleSheet.create({
  container: {
    minHeight: 60,
    width: "100%",
    paddingHorizontal: 16,
    backgroundColor: AppTheme.backgroundColor,
    fontFamily: CustomFont.PoppinsBold,
    alignContent: "center",
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    marginHorizontal: 16,
    flexShrink: 1,
    flexGrow: 1,
    marginVertical: 8,
    fontFamily: CustomFont.PoppinsMedium,
    textAlignVertical: "center",
    color: AppTheme.primaryTextColor,
  },
});
