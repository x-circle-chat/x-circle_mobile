import { Image, View, StyleSheet } from "react-native";
import AppTheme from "../../../../../constants/AppTheme";
import AssetPath from "../../../../../constants/AssetPath";

const StoryTile = ({ isAdd, storyImage }) => {
  return (
    <View style={{ height: "100%", aspectRatio: 1, padding: 2 }}>
      {isAdd && (
        <View style={styles.container}>
          <Image
            style={{
              height: "90%",
              width: "90%",
            }}
            source={AssetPath.add}
          />
        </View>
      )}
      {!isAdd && (
        <View style={styles.container}>
          <View style={styles.borderView}>
            <Image
              style={{
                height: "90%",
                width: "90%",
              }}
              source={storyImage}
            />
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    //backgroundColor: AppTheme.borderColor,
    justifyContent: "center",
    alignItems: "center",
  },
  borderView: {
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    width: "100%",
    borderWidth: 2,
    borderColor: AppTheme.themeColor,
    borderRadius: 1000,
  },
});

export default StoryTile;
