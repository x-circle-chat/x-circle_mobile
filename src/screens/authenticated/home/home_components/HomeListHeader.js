import { Image, StyleSheet, Text, TextInput, View } from "react-native";
import AppTheme from "../../../../constants/AppTheme";
import CustomFont from "../../../../constants/CustomFont";
import AssetPath from "../../../../constants/AssetPath";
import Strings from "../../../../constants/Strings";

const HomeListHeader = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.text1}>{Strings.message}</Text>
      <View style={styles.containerArchive}>
        <Text style={styles.text2}>{Strings.archive}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 40,
    width: "100%",
    alignContent: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  text1: {
    color: AppTheme.primaryTextColor,
    fontFamily: CustomFont.PoppinsMedium,
    fontSize: 18,
    textAlign: "left",
  },
  containerArchive: {
    height: 29,
    paddingHorizontal: 12,
    backgroundColor: AppTheme.archiveBG,
    alignContent: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 15,
    borderColor: AppTheme.themeColor,
    borderWidth: 1,
  },
  text2: {
    color: AppTheme.primaryTextColor,
    fontFamily: CustomFont.PoppinsMedium,
    fontSize: 10,
    textAlign: "left",
  },
});

export default HomeListHeader;
