import { StyleSheet, View, Pressable, Image, Text } from "react-native";
import AppTheme from "../../../../constants/AppTheme";
import CustomFont from "../../../../constants/CustomFont";
import AssetPath from "../../../../constants/AssetPath";
import GapView from "../../../../components/GapView";

const ChatNavigationHeader = ({ navigation }) => {
  return (
    <View style={styles.navigationBar}>
      <Pressable
        style={{
          paddingLeft: 16,
          width: 40,
          justifyContent: "center",
        }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Image
          style={{
            height: 12,
            width: 12,
          }}
          source={AssetPath.back}
        />
      </Pressable>
      <View style={styles.imageTextContainer}>
        <Image
          style={{
            height: 50,
            width: 50,
          }}
          source={AssetPath.story1}
        />
        <GapView width={16} />
        <View style={styles.columnContainer}>
          <Text style={styles.title}>{"Smith Mathew"}</Text>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Text style={styles.subtitle}>{"Active Now"}</Text>
            <GapView width={2} />

            <View
              style={{
                width: 8,
                height: 8,
                backgroundColor: AppTheme.themeColor,
                borderRadius: 1000,
                marginBottom: 1.5,
              }}
            />
          </View>
        </View>
      </View>
      <View style={{ flexDirection: "row", paddingRight: 16 }}>
        <Image
          style={{
            height: 20,
            width: 15,
          }}
          source={AssetPath.phone}
        />
        <GapView width={16} />
        <Image
          style={{
            height: 17,
            width: 24,
          }}
          source={AssetPath.camera}
        />
      </View>
    </View>
  );
};
export default ChatNavigationHeader;

const styles = StyleSheet.create({
  navigationBar: {
    width: "100%",
    flexDirection: "row",
    alignContent: "center",
    alignItems: "center",
  },
  imageTextContainer: {
    paddingVertical: 12,
    alignContent: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    flexShrink: 1,
    flex: 1,
  },
  columnContainer: {
    flexShrink: 1,
    justifyContent: "center",
  },
  title: {
    color: AppTheme.primaryTextColor,
    fontFamily: CustomFont.PoppinsMedium,
    fontSize: 18,
    textAlign: "left",
  },
  subtitle: {
    textAlign: "left",
    color: AppTheme.subtitleTextColor,
    fontFamily: CustomFont.PoppinsRegular,
    fontSize: 10,
  },
});
