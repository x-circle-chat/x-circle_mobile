import { StyleSheet, TextInput, View } from "react-native";
import AppTheme from "../constants/AppTheme";
import CustomFont from "../constants/CustomFont";
//const italicFont = require("../assets/fonts/Poppins/Poppins-ExtraBoldItalic.ttf");

const InputField = ({ placeholder }) => {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        //onChangeText={onChangeNumber}
        //value={number}
        underlineColorAndroid="transparent"
        borderWidth={0}
        InputProps={{ disableUnderline: true }}
        placeholder={placeholder}
        placeholderTextColor={AppTheme.lightTextColor}
        keyboardType="visible-password"
        autoCorrect={false}
        spellCheck={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 40,
    borderRadius: 20,
    width: "100%",
    borderWidth: 2,
    paddingHorizontal: 16,
    borderColor: AppTheme.themeColor,
    fontFamily: CustomFont.PoppinsBold,
    justifyContent: "center",
  },
  input: {
    fontFamily: CustomFont.PoppinsMedium,
    textAlignVertical: "center",
    color: AppTheme.primaryTextColor,
  },
});

export default InputField;

//keyboardType="visible-password"
