const AssetPath = {
  appLogo: require("../assets/images/app_logo.png"),
  back: require("../assets/images/back.png"),
  attach: require("../assets/images/attach.png"),
  add: require("../assets/images/add.png"),
  camera: require("../assets/images/camera.png"),
  contact: require("../assets/images/contact.svg"),
  explore: require("../assets/images/explore.svg"),
  feed: require("../assets/images/feed.svg"),
  home: require("../assets/images/home.svg"),
  phone: require("../assets/images/phone.png"),
  record_audio: require("../assets/images/record_audio.png"),
  record: require("../assets/images/record.png"),
  search: require("../assets/images/search.png"),
  send_message: require("../assets/images/send_message.png"),
  settings: require("../assets/images/settings.svg"),
  story1: require("../assets/images/temp/story1.png"),
  story2: require("../assets/images/temp/story2.png"),
  story3: require("../assets/images/temp/story3.png"),
  story4: require("../assets/images/temp/story4.png"),
  story5: require("../assets/images/temp/story5.png"),
};

export default AssetPath;
