import { Image, StyleSheet, Text, TextInput, View } from "react-native";
import AppTheme from "../../../../constants/AppTheme";
import CustomFont from "../../../../constants/CustomFont";
import AssetPath from "../../../../constants/AssetPath";
import Strings from "../../../../constants/Strings";

const HomeSectionHeader = ({ sectionTitle }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{sectionTitle}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 40,
    width: "100%",
    alignContent: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: AppTheme.whiteColorDynamic,
  },
  text: {
    color: AppTheme.primaryTextColor,
    fontFamily: CustomFont.PoppinsMedium,
    fontSize: 14,
    textAlign: "left",
  },
});

export default HomeSectionHeader;
