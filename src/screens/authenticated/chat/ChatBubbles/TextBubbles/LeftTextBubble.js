import { Dimensions, StyleSheet, View, Text, Image } from "react-native";
import AppTheme from "../../../../../constants/AppTheme";
import CustomFont from "../../../../../constants/CustomFont";
import AssetPath from "../../../../../constants/AssetPath";

const LeftTextBubble = ({ navigation, text }) => {
  return (
    <View style={styles.container}>
      <View>
        <Image
          style={{
            height: 50,
            width: 50,
          }}
          source={AssetPath.story1}
        />
        <View
          style={{
            top: 4,
            right: 4,
            position: "absolute",
            borderRadius: 1000,
            height: 8,
            width: 8,
            backgroundColor: AppTheme.themeColor,
          }}
        ></View>
      </View>
      <View style={styles.bubble}>
        <Text style={styles.text}>{text}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 16,
    alignItems: "flex-end",
    flexDirection: "row",
  },
  bubble: {
    backgroundColor: AppTheme.borderGrayColor,
    minHeight: 32,
    maxWidth: "70%",
    marginLeft: 12,
    marginBottom: 4,
    paddingHorizontal: 16,
    paddingVertical: 4,
    justifyContent: "center",
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    borderBottomRightRadius: 16,
  },
  text: {
    color: AppTheme.subtitleTextColor,
    fontFamily: CustomFont.PoppinsRegular,
    fontSize: 14,
  },
});

export default LeftTextBubble;
