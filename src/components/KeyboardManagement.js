import { KeyboardAvoidingView, ScrollView } from "react-native";

const KeyboardManagement = ({
  behavior = "height",
  children,
  encloseInScroll = true,
}) => {
  return (
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      behavior={Platform.OS === "ios" ? behavior : undefined}
      keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 0}
    >
      {encloseInScroll && (
        <ScrollView
          keyboardShouldPersistTaps="handled"
          bounces={false}
          contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
        >
          {children}
        </ScrollView>
      )}
      {!encloseInScroll && children}
    </KeyboardAvoidingView>
  );
};

export default KeyboardManagement;
