import { useState } from "react";
import { TextInput } from "react-native";

export default function AutoGrowTextInput({
  value,
  onChangeText,
  maxHeight = 150,
  style,
  placeholder,
  keyboardType,
  returnKeyType,
  blurOnSubmit,
  onSubmitEditing,
  selectionColor,
  placeholderTextColor,
}) {
  const [height, setHeight] = useState();

  return (
    <TextInput
      style={[style, { height: height > maxHeight ? maxHeight : height }]}
      onChangeText={onChangeText}
      value={value}
      underlineColorAndroid="transparent"
      borderWidth={0}
      InputProps={{ disableUnderline: true }}
      placeholder={placeholder}
      selectionColor={selectionColor}
      placeholderTextColor={placeholderTextColor}
      keyboardType={keyboardType}
      multiline={true}
      returnKeyType={returnKeyType}
      blurOnSubmit={blurOnSubmit}
      onSubmitEditing={onSubmitEditing}
      onContentSizeChange={(e) => setHeight(e.nativeEvent.contentSize.height)}
    />
  );
}
