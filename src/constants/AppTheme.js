const AppTheme = {
  isLightTheme: true,

  get themeColor() {
    return this.isLightTheme ? "#24B47E" : "#24B47E";
  },
  get backgroundColor() {
    return this.isLightTheme ? "#FFFFFF" : "#181818";
  },

  get cardBackgroundColor() {
    return this.isLightTheme ? "#fafafa" : "#212121";
  },

  get primaryTextColor() {
    return this.isLightTheme ? "#3A3A3A" : "#FDFDFD";
  },

  get subtitleTextColor() {
    return this.isLightTheme ? "#9C9797" : "#666666";
  },

  get lightTextColor() {
    return this.isLightTheme ? "#989898" : "#989898";
  },

  get borderColor() {
    return this.isLightTheme ? "#F6F6F6" : "#2A2A2A";
  },

  get borderGrayColor() {
    return "#E4E4E4";
  },

  get inputBGColor() {
    return this.isLightTheme ? "#F6F6F6" : "#323232";
  },

  get inputBorderColor() {
    return this.isLightTheme ? "#F6F6F6" : "#3E3E3E";
  },

  get whiteColor() {
    return this.isLightTheme ? "#000000" : "#FFFFFF";
  },

  get whiteColorDynamic() {
    return this.isLightTheme ? "#FFFFFF" : "#000000";
  },

  get alwaysWhiteColor() {
    return "#FFFFFF";
  },

  get blackColor() {
    return "#000000";
  },

  get iconColor() {
    return "#A8A8A8";
  },

  get archiveBG() {
    return "rgba(85, 169, 157, 0.3)";
  },
};

export default AppTheme;
