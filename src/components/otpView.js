import { StyleSheet, View } from "react-native";
import OTPTextView from "react-native-otp-textinput";
import { Colors } from "react-native/Libraries/NewAppScreen";
import AppTheme from "../constants/AppTheme";

const OtpView = () => {
  return (
    <View style={styles.container}>
      <OTPTextView
        inputCount={6}
        autoFocus={false}
        textInputStyle={{
          width: 35,
          fontSize: 20,
          borderBottomWidth: 2,
          color: AppTheme.whiteColor,
        }}
        containerStyle={{
          width: "100%",
        }}
      ></OTPTextView>
    </View>
  );
};

export default OtpView;

const styles = StyleSheet.create({
  container: {
    height: 40,
    width: "100%",
    marginBottom: 32,
    alignContent: "center",
    justifyContent: "center",
  },
});
