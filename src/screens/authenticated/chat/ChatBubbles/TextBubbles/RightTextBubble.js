import { StyleSheet, View, Text } from "react-native";
import AppTheme from "../../../../../constants/AppTheme";
import CustomFont from "../../../../../constants/CustomFont";

const RightTextBubble = ({ navigation, text }) => {
  return (
    <View style={styles.container}>
      <View style={styles.bubble}>
        <Text style={styles.text}>{text}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "flex-end",
  },
  bubble: {
    margin: 16,
    backgroundColor: AppTheme.archiveBG,
    minHeight: 32,
    maxWidth: "70%",
    paddingHorizontal: 16,
    paddingVertical: 4,
    justifyContent: "center",
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    borderBottomLeftRadius: 16,
  },
  text: {
    color: AppTheme.themeColor,
    fontFamily: CustomFont.PoppinsRegular,
    fontSize: 14,
  },
});

export default RightTextBubble;
