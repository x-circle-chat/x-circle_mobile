import { StyleSheet, View } from "react-native";
import AppTheme from "../../../constants/AppTheme";
import CustomFont from "../../../constants/CustomFont";
import { SafeAreaView } from "react-native-safe-area-context";
import KeyboardManagement from "../../../components/KeyboardManagement";
import MessagesView from "./chat_components/MessagesView";
import ChatNavigationHeader from "./chat_components/ChatNavigationHeader";
import ChatInputView from "./chat_components/ChatInputView";

const ChatScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <KeyboardManagement behavior="padding" encloseInScroll={false}>
        <ChatNavigationHeader navigation={navigation} />
        <View
          style={{
            width: "100%",
            height: 1,
            backgroundColor: AppTheme.borderGrayColor,
          }}
        />
        {MessagesView()}
        <ChatInputView />
      </KeyboardManagement>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppTheme.backgroundColor,
  },
});

export default ChatScreen;
