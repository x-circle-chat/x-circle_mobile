import { Image, StyleSheet, TextInput, View } from "react-native";
import AppTheme from "../../../../constants/AppTheme";
import CustomFont from "../../../../constants/CustomFont";
import AssetPath from "../../../../constants/AssetPath";
import Strings from "../../../../constants/Strings";

const MessagesSearchView = () => {
  return (
    <View style={styles.container}>
      <Image
        style={{
          width: 18,
          height: 18,
        }}
        source={AssetPath.search}
      />
      <TextInput
        style={styles.input}
        //onChangeText={onChangeNumber}
        //value={number}
        underlineColorAndroid="transparent"
        borderWidth={0}
        InputProps={{ disableUnderline: true }}
        placeholder={Strings.searchHere}
        selectionColor={AppTheme.primaryTextColor}
        placeholderTextColor={AppTheme.lightTextColor}
        keyboardType="visible-password"
        autoCorrect={false}
        spellCheck={false}
      />
      <Image
        style={{
          width: 18,
          height: 18,
        }}
        resizeMode="contain"
        source={AssetPath.record}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 40,
    borderRadius: 20,
    width: "100%",
    paddingHorizontal: 16,
    backgroundColor: AppTheme.inputBGColor,
    fontFamily: CustomFont.PoppinsBold,
    alignContent: "center",
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    marginHorizontal: 8,
    flexGrow: 1,
    height: "100%",
    fontFamily: CustomFont.PoppinsMedium,
    textAlignVertical: "center",
    color: AppTheme.primaryTextColor,
  },
});

export default MessagesSearchView;
