const Routes = {
  login: "login",
  otp: "otp",
  home: "home",
  chat: "chat",
};

export default Routes;
