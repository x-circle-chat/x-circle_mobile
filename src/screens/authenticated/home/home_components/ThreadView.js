import {
  Image,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";
import AppTheme from "../../../../constants/AppTheme";
import CustomFont from "../../../../constants/CustomFont";
import AssetPath from "../../../../constants/AssetPath";
import Strings from "../../../../constants/Strings";
import GapView from "../../../../components/GapView";

const ThreadView = ({ item, onPress }) => {
  return (
    <Pressable onPress={onPress}>
      <View style={styles.container}>
        <Image
          style={{
            height: 50,
            width: 50,
          }}
          source={AssetPath.story1}
        />
        <GapView width={16} />
        <View style={styles.columnContainer}>
          <View style={styles.topContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{item}</Text>
              <GapView width={4} />
              <View style={styles.countContainer}>
                <Text style={styles.countLabel}>{"2"}</Text>
              </View>
            </View>
            <Text style={styles.dateLabel}>{"29 march"}</Text>
          </View>
          <Text style={styles.subtitle}>
            {"Let’s get back to the work, You Let’s get back to the work."}
          </Text>
        </View>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 12,
    alignContent: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: AppTheme.whiteColorDynamic,
  },
  columnContainer: {
    flexShrink: 1,
  },
  title: {
    color: AppTheme.primaryTextColor,
    fontFamily: CustomFont.PoppinsMedium,
    fontSize: 18,
    textAlign: "left",
    flexShrink: 1,
  },
  subtitle: {
    textAlign: "left",
    color: AppTheme.subtitleTextColor,
    fontFamily: CustomFont.PoppinsRegular,
    fontSize: 16,
  },
  topContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    // backgroundColor: AppTheme.themeColor,
  },
  titleContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    flexShrink: 1,
  },
  countContainer: {
    minWidth: 15,
    aspectRatio: 1,
    paddingHorizontal: 2,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: AppTheme.themeColor,
    borderRadius: 10000,
  },
  countLabel: {
    color: AppTheme.alwaysWhiteColor,
    fontFamily: CustomFont.PoppinsBold,
    fontSize: 10,
  },
  dateLabel: {
    marginLeft: 16,
    color: AppTheme.lightTextColor,
    fontFamily: CustomFont.PoppinsRegular,
    fontSize: 14,
  },
});

export default ThreadView;
