import { Image, StyleSheet, Text, TextInput, View } from "react-native";
import InputField from "../../components/input_field";
import AppTheme from "../../constants/AppTheme";
import Strings from "../../constants/Strings";
import AssetPath from "../../constants/AssetPath";
import CustomFont from "../../constants/CustomFont";
import CustomButton from "../../components/custom_button";
import OtpView from "../../components/otpView";
import KeyboardManagement from "../../components/KeyboardManagement";
import Routes from "../../constants/routes";

const OtpScreen = ({ navigation }) => {
  return (
    <KeyboardManagement>
      <View style={styles.container}>
        <View style={styles.cardContainer}>
          <Image style={styles.logoImage} source={AssetPath.appLogo} />
          <Text style={styles.headline}>
            {Strings.enterOTPHeadline + "puri@gmail.com"}
          </Text>
          <OtpView></OtpView>
          <CustomButton
            title={Strings.submitButton}
            onPress={() => {
              navigation.navigate(Routes.home);
            }}
          ></CustomButton>
        </View>
      </View>
    </KeyboardManagement>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppTheme.backgroundColor,
    alignItems: "center",
    justifyContent: "center",
  },
  cardContainer: {
    backgroundColor: AppTheme.cardBackgroundColor,
    alignItems: "center",
    justifyContent: "center",
    width: "90%",
    paddingHorizontal: 24,
    paddingTop: 24,
    borderRadius: 8,
  },
  logoImage: {
    width: 80,
    height: 80,
  },
  headline: {
    width: "100%",
    paddingTop: 40,
    paddingBottom: 20,
    color: AppTheme.lightTextColor,
    fontFamily: CustomFont.PoppinsMedium,
    fontSize: 13,
    textAlign: "left",
  },
});

export default OtpScreen;
