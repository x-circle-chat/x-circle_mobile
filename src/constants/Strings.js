const Strings = {
  appName: "X-CIRCLE",

  //MENU
  home: "Home",
  explore: "Explore",
  feed: "Feed",
  settings: "Settings",
  contact: "Contact",

  //Login
  loginButton: "Login",
  enterEmailHeadline: "Please enter your email address for login",
  enterEmail: "Enter email address",

  //Login
  submitButton: "Submit",
  enterOTPHeadline: "Please enter OTP sent to ",

  //Home/Chat
  chat: "Chat",
  searchHere: "Search here...",

  message: "Message",
  archive: "Archive Chat",

  sendMessage: "Send Message",
};

export default Strings;
