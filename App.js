import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import LoginScreen from "./src/screens/onboarding/login";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import OtpScreen from "./src/screens/onboarding/otp";
import Routes from "./src/constants/routes";
import HomeScreen from "./src/screens/authenticated/home/home";
import ChatScreen from "./src/screens/authenticated/chat/chat_screen";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name={Routes.login} component={LoginScreen} />
        <Stack.Screen name={Routes.otp} component={OtpScreen} />
        <Stack.Screen name={Routes.home} component={HomeScreen} />
        <Stack.Screen name={Routes.chat} component={ChatScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
