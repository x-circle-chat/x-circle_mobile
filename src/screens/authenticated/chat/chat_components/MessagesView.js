import { SectionList, StyleSheet, View, Text } from "react-native";
import AppTheme from "../../../../constants/AppTheme";
import CustomFont from "../../../../constants/CustomFont";
import HomeSectionHeader from "../../home/home_components/HomeSectionHeader";
import ThreadView from "../../home/home_components/ThreadView";
import LeftTextBubble from "../ChatBubbles/TextBubbles/LeftTextBubble";
import RightTextBubble from "../ChatBubbles/TextBubbles/RightTextBubble";
import ChatHeader from "../ChatBubbles/ChatHeader";

const DATA = [
  {
    title: "Thursday 24, 2022",
    data: [
      "Are you still travelling?",
      "Yes, i’m at Istanbul",
      "OoOo, Thats so Cool!",
    ],
  },
  {
    title: "Thursday 24, 2022",
    data: [
      "Raining?? Yes You need to come at sharp at this time.",
      "Hi, Did you heared? Ok do not messh with me i am not alone. Ok do not messh with me i am not alone. Ok do not messh with me i am not alone",
      "Ok!",
    ],
  },
  {
    title: "Thursday 24, 2022",
    data: [
      "Are you still travelling?",
      "Yes, i’m at Istanbul",
      "OoOo, Thats so Cool!",
    ],
  },
  {
    title: "Thursday 24, 2022",
    data: ["Raining??", "Hi, Did you heared? But we can do nothing.", "Ok!"],
  },
];

const MessagesView = () => {
  return (
    <View style={styles.container}>
      <SectionList
        sections={DATA}
        keyExtractor={(item, index) => item + index}
        renderItem={({ index, item }) => {
          return index % 2 == 0 ? (
            <LeftTextBubble text={item} />
          ) : (
            <RightTextBubble text={item} />
          );
        }}
        renderSectionHeader={({ section: { title } }) => (
          <ChatHeader title={title}></ChatHeader>
        )}
      />
    </View>
  );
};
export default MessagesView;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flex: 1,
    backgroundColor: AppTheme.backgroundColor,
  },
  safeContainer: {
    flex: 1,
    marginHorizontal: 16,
  },
  header: {
    width: "100%",
    paddingTop: 16,
    paddingBottom: 12,
    color: AppTheme.primaryTextColor,
    fontFamily: CustomFont.PoppinsMedium,
    fontSize: 20,
    textAlign: "left",
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
  },
  title: {
    fontSize: 24,
  },
});
