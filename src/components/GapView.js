import { View } from "react-native";

const GapView = ({ width = 0, height = 0 }) => {
  return <View style={{ height: height, width: width }} />;
};

export default GapView;
