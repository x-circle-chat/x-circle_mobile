import {
  FlatList,
  Image,
  Text,
  StyleSheet,
  TextInput,
  View,
} from "react-native";
import AppTheme from "../../../../../constants/AppTheme";
import AssetPath from "../../../../../constants/AssetPath";
import StoryTile from "./StoryTile";

const HomeStoriesView = ({}) => {
  return (
    <View style={styles.container}>
      <FlatList
        horizontal={true}
        data={[
          { key: "1", image: AssetPath.story1 },
          { key: "2", image: AssetPath.story1 },
          { key: "3", image: AssetPath.story2 },
          { key: "4", image: AssetPath.story3 },
          { key: "5", image: AssetPath.story4 },
          { key: "6", image: AssetPath.story5 },
        ]}
        renderItem={({ item }) => (
          <StoryTile
            isAdd={item.key == "1"}
            storyImage={item.image}
          ></StoryTile>
        )}
        keyExtractor={(item) => item.key}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    aspectRatio: 6,
    width: "100%",
  },
});

export default HomeStoriesView;
