import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  SectionList,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import AppTheme from "../../../constants/AppTheme";
import Strings from "../../../constants/Strings";
import CustomFont from "../../../constants/CustomFont";
import MessagesSearchView from "./home_components/MessagesSearchView";
import HomeStoriesView from "./home_components/HomeStoriesView/HomeStoriesView";
import GapView from "../../../components/GapView";
import HomeListHeader from "./home_components/HomeListHeader";
import HomeSectionHeader from "./home_components/HomeSectionHeader";
import ThreadView from "./home_components/ThreadView";
import Routes from "../../../constants/routes";

const HomeScreen = ({ navigation }) => {
  const DATA = [
    {
      title: "Main dishes",
      data: ["Pizza", "Burger", "Risotto"],
    },
    {
      title: "Sides",
      data: ["French Fries", "Onion Rings", "Fried Shrimps"],
    },
    {
      title: "Drinks",
      data: ["Water", "Coke", "Beer"],
    },
    {
      title: "Desserts",
      data: ["Cheese Cake", "Ice Cream"],
    },
  ];

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.safeContainer}>
        <Text style={styles.header}>{Strings.chat}</Text>
        <MessagesSearchView></MessagesSearchView>
        <GapView height={16} />
        <HomeStoriesView></HomeStoriesView>
        <GapView height={12} />
        <HomeListHeader></HomeListHeader>
        <SectionList
          sections={DATA}
          keyExtractor={(item, index) => item + index}
          renderItem={({ item }) => (
            <ThreadView
              item={item}
              onPress={() => {
                navigation.navigate(Routes.chat);
              }}
            ></ThreadView>
          )}
          renderSectionHeader={({ section: { title } }) => (
            <HomeSectionHeader sectionTitle={title}></HomeSectionHeader>
          )}
          renderSectionFooter={({ section: { title } }) => (
            <View
              style={{
                backgroundColor: AppTheme.borderGrayColor,
                height: 1,
                width: "100%",
              }}
            ></View>
          )}
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppTheme.backgroundColor,
  },
  safeContainer: {
    flex: 1,
    marginHorizontal: 16,
  },
  header: {
    width: "100%",
    paddingTop: 16,
    paddingBottom: 12,
    color: AppTheme.primaryTextColor,
    fontFamily: CustomFont.PoppinsMedium,
    fontSize: 20,
    textAlign: "left",
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
  },
  title: {
    fontSize: 24,
  },
});

export default HomeScreen;
