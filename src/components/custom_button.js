import { Button, Pressable, StyleSheet, Text, View } from "react-native";
import AppTheme from "../constants/AppTheme";
import CustomFont from "../constants/CustomFont";

const CustomButton = ({ title, onPress }) => {
  return (
    <Pressable
      onPress={onPress}
      style={({ pressed }) => {
        return [styles.press, pressed && { opacity: 0.5 }];
      }}
    >
      <View style={styles.container}>
        <Text style={styles.text}>{title}</Text>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  press: {
    height: 40,
    borderRadius: 20,
    width: "100%",
    backgroundColor: AppTheme.themeColor,
    marginVertical: 24,
  },
  container: {
    height: "100%",
    borderRadius: 20,
    width: "100%",
    justifyContent: "center",
    alignContent: "center",
  },
  text: {
    fontFamily: CustomFont.PoppinsBold,
    textAlign: "center",
    color: AppTheme.alwaysWhiteColor,
  },
});

//keyboardType="visible-password"

export default CustomButton;
